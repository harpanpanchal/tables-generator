(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

// Variables
var excelDoc = XLSX;
var HTMLOUT = document.getElementById('output');
var btnCopy = document.getElementById('btnCopy');
var loader = document.getElementById('loader');

var globalWb = void 0;
var htmlstr = void 0;
var filesLen = void 0;
var totalCount = void 0;

// Define new values
totalCount = 0;
HTMLOUT.innerHTML = '';
// Document Processing, html table code stored in the variable and displayed on the screen
var processDoc = function () {
    var toHtml = function toHtml(workbook) {
        workbook.SheetNames.forEach(function (sheetName) {
            htmlstr = '<div class=\'sheetname\'>' + sheetName + '</div>';
            htmlstr += excelDoc.write(workbook, { sheet: sheetName, type: 'binary', bookType: 'html' });
            HTMLOUT.innerHTML += htmlstr;
        });
        totalCount++;
        if (totalCount === filesLen) {
            // removing the pre-generated 'id' and 't' attributes inside the table cells
            var elems = document.querySelectorAll('#output table tr td');
            for (var i = 0; i < elems.length; i++) {
                elems[i].removeAttribute('id');
                elems[i].removeAttribute('t');
                elems[i].removeAttribute('xml:space');
            }
            // creating TH element in each and every tables
            var tableAll = document.getElementById('output').querySelectorAll('table');
            for (var _i = 0; _i < tableAll.length; ++_i) {
                tableAll[_i].createTHead();
            }
            // Moving first row from Tbody section to THead section in the table
            var divs = document.querySelectorAll('#output table');
            for (var _i2 = 0; _i2 < divs.length; _i2++) {
                $('#output table:eq(' + _i2 + ') tbody tr:eq(0)').prependTo($('#output table:eq(' + _i2 + ') thead')); // move first tr to thead
            }
            // Replacing TD with TH in every row inside Thead section
            $('#output table thead').each(function replaceText() {
                var text = $(this).html();
                $(this).html(text.replace(/<td /g, '<th '));
            });
            $('#output table thead').each(function replaceText() {
                var text = $(this).html();
                $(this).html(text.replace(/td>/g, 'th>'));
            });
            // Adding class names dynamically if the cell values contain '{class:' in the table
            $('#output table thead tr th, #output table tbody tr td').each(function replaceText() {
                var text = $(this).html();
                if (text.indexOf('{class:') >= 0) {
                    var tdVal = text.split(':');
                    var final = tdVal[1].split('}');
                    $(this).addClass(final[0]);
                    $(this).html(final[1].trim());
                }
            });
            // Creating copy buttons for each table on the screen
            for (var _i3 = 0; _i3 < tableAll.length; ++_i3) {
                if (tableAll.length > 1) {
                    var btnTable = document.createElement('button');
                    btnTable.className = 'copyTable btn';
                    btnTable.innerHTML = 'Copy Table';
                    btnTable.addEventListener('click', copyTable);
                    $(tableAll[_i3]).before(btnTable);
                }
            }
            // Displaying the final output on the screen
            HTMLOUT.classList.remove('hide');
            // Hiding loader icon once the final output is shown on the screen
            var loaderCls = loader.classList;
            if (!loaderCls.contains('hide')) loader.className += 'hide';
            var btnCopyclass = btnCopy.classList;
            if (btnCopyclass.contains('hide')) {
                btnCopy.classList.remove('hide');
            }
        }
    };
    return function processDoc(wBook) {
        globalWb = wBook;
        var output = '';
        output = toHtml(wBook);
    };
}();

var readFile = function () {
    return function readFile(files) {
        totalCount = 0;
        if (files.length > 0) {
            HTMLOUT.innerHTML = '';
            filesLen = files.length;
            var loaderCls = loader.classList;
            if (loaderCls.contains('hide')) {
                loader.classList.remove('hide');
            }
        }

        var _loop = function _loop(i, f) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var data = e.target.result;
                data = new Uint8Array(data);
                var fileCaption = files[i].name.split('.');
                fileCaption[0] = fileCaption[0].replace(/\s+/g, '-').toLowerCase();
                HTMLOUT.innerHTML += '<div class=\'fileCaption\'>' + fileCaption[0] + '</div>';
                processDoc(excelDoc.read(data, { type: 'array' }));
                $('#output table').each(function addElement() {
                    var attr = $(this).attr('data-filename');
                    if (!(attr && attr !== true)) {
                        $(this).attr('data-filename', fileCaption[0]);
                    }
                });
            };
            reader.readAsArrayBuffer(f);
        };

        for (var i = 0, f; f = files[i]; i++) {
            _loop(i, f);
        }
    };
}();

// Remove file extension from the file name & store in the textbox
var excelFile = function excelFile() {
    var xlf = document.getElementById('xlf');
    if (!xlf.addEventListener) return;

    function handleFile(e) {
        readFile(e.target.files);
    }
    xlf.addEventListener('change', handleFile, false);
};
excelFile();
// Assign even listener to the browse button to enable it to click the file input box. File input box is kept hidden for UI decoration purpose.
document.querySelector('#browseBtn').addEventListener('click', function () {
    return document.getElementById('xlf').click();
});
// Copy button function to copy the code from the output element DIV
document.querySelector('#btnCopy').addEventListener('click', function () {
    document.querySelector('#copyTxt').value = '';
    if (HTMLOUT.innerHTML !== '') {
        var finalCopy = '';
        var outputTable = document.querySelectorAll('#output table');
        for (var i = 0; i < outputTable.length; ++i) {
            finalCopy += '<table>' + outputTable[i].innerHTML + '</table>';
        }
        document.querySelector('#copyTxt').value = finalCopy;
        document.querySelector('#copyTxt').select();
        document.execCommand('Copy');
    }
});
// CopyTable function to copy the individual table code
function copyTable() {
    document.querySelector('#copyTxt').value = '';
    if (HTMLOUT.innerHTML !== '') {
        var finalCopy = '';
        finalCopy += '<table>' + this.nextSibling.innerHTML + '</table>';
        document.querySelector('#copyTxt').value = finalCopy;
        document.querySelector('#copyTxt').select();
        document.execCommand('Copy');
    }
}
// Download code in text file format when clicking download button
document.querySelector('#btnDownload').addEventListener('click', function () {
    if (HTMLOUT.innerHTML !== '') {
        var fileNameToSaveAs = void 0;
        // Get the element with id="myDIV" (a div), then get all p elements with class="example" inside div
        var fileCaption = document.querySelectorAll('#output .fileCaption');
        for (var i = 0; i < fileCaption.length; ++i) {
            var outputTable = document.querySelectorAll('#output table[data-filename=' + fileCaption[i].innerHTML + ']');

            var _loop2 = function _loop2(j) {
                // File names will be kept as  file and sheet name for better understanding
                if (fileCaption.length === 1 && outputTable.length === 1) {
                    fileNameToSaveAs = fileCaption[i].innerHTML + '-' + outputTable[j].previousSibling.innerHTML;
                } else {
                    fileNameToSaveAs = fileCaption[i].innerHTML + '-' + outputTable[j].previousSibling.previousSibling.innerHTML;
                }
                var textToSave = '<table>' + outputTable[j].innerHTML + '</table>';
                // Create a new Blob object using the
                //response data of the onload object
                var textToSaveAsBlob = new Blob([textToSave], { type: 'text/plain' });
                //Create a link element, hide it, direct
                //it towards the blob, and then 'click' it programatically
                var downloadLink = document.createElement('a');
                downloadLink.style = "display: none";
                document.body.appendChild(downloadLink);
                //Create a DOMString representing the blob
                //and point the link element towards it
                var textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);
                downloadLink.href = textToSaveAsURL;
                downloadLink.download = fileNameToSaveAs;
                downloadLink.innerHTML = 'Download File';
                //programatically click the link to trigger the
                setTimeout(function () {
                    downloadLink.click();
                    console.log("Download");
                    document.body.removeChild(downloadLink);
                }, 1000);
            };

            for (var j = 0; j < outputTable.length; j++) {
                _loop2(j);
            }
        }
    } else {
        alert('Please select the file you want to convert to a table.');
    }
});

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzY3JpcHRzL3NvdXJjZS9tYWluLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7QUNBQTtBQUNBLElBQU0sV0FBVyxJQUFqQjtBQUNBLElBQU0sVUFBVSxTQUFTLGNBQVQsQ0FBd0IsUUFBeEIsQ0FBaEI7QUFDQSxJQUFNLFVBQVUsU0FBUyxjQUFULENBQXdCLFNBQXhCLENBQWhCO0FBQ0EsSUFBTSxTQUFTLFNBQVMsY0FBVCxDQUF3QixRQUF4QixDQUFmOztBQUVBLElBQUksaUJBQUo7QUFDQSxJQUFJLGdCQUFKO0FBQ0EsSUFBSSxpQkFBSjtBQUNBLElBQUksbUJBQUo7O0FBRUE7QUFDQSxhQUFhLENBQWI7QUFDQSxRQUFRLFNBQVIsR0FBb0IsRUFBcEI7QUFDQTtBQUNBLElBQU0sYUFBYyxZQUFNO0FBQ3RCLFFBQU0sU0FBUyxTQUFTLE1BQVQsQ0FBZ0IsUUFBaEIsRUFBMEI7QUFDckMsaUJBQVMsVUFBVCxDQUFvQixPQUFwQixDQUE0QixVQUFDLFNBQUQsRUFBZTtBQUN2QyxvREFBb0MsU0FBcEM7QUFDQSx1QkFBVyxTQUFTLEtBQVQsQ0FBZSxRQUFmLEVBQXlCLEVBQUUsT0FBTyxTQUFULEVBQW9CLE1BQU0sUUFBMUIsRUFBb0MsVUFBVSxNQUE5QyxFQUF6QixDQUFYO0FBQ0Esb0JBQVEsU0FBUixJQUFxQixPQUFyQjtBQUNILFNBSkQ7QUFLQTtBQUNBLFlBQUksZUFBZSxRQUFuQixFQUE2QjtBQUN6QjtBQUNBLGdCQUFNLFFBQVEsU0FBUyxnQkFBVCxDQUEwQixxQkFBMUIsQ0FBZDtBQUNBLGlCQUFLLElBQUksSUFBSSxDQUFiLEVBQWdCLElBQUksTUFBTSxNQUExQixFQUFrQyxHQUFsQyxFQUF1QztBQUNuQyxzQkFBTSxDQUFOLEVBQVMsZUFBVCxDQUF5QixJQUF6QjtBQUNBLHNCQUFNLENBQU4sRUFBUyxlQUFULENBQXlCLEdBQXpCO0FBQ0Esc0JBQU0sQ0FBTixFQUFTLGVBQVQsQ0FBeUIsV0FBekI7QUFDSDtBQUNEO0FBQ0EsZ0JBQU0sV0FBVyxTQUFTLGNBQVQsQ0FBd0IsUUFBeEIsRUFBa0MsZ0JBQWxDLENBQW1ELE9BQW5ELENBQWpCO0FBQ0EsaUJBQUssSUFBSSxLQUFJLENBQWIsRUFBZ0IsS0FBSSxTQUFTLE1BQTdCLEVBQXFDLEVBQUUsRUFBdkMsRUFBMEM7QUFDdEMseUJBQVMsRUFBVCxFQUFZLFdBQVo7QUFDSDtBQUNEO0FBQ0EsZ0JBQU0sT0FBTyxTQUFTLGdCQUFULENBQTBCLGVBQTFCLENBQWI7QUFDQSxpQkFBSyxJQUFJLE1BQUksQ0FBYixFQUFnQixNQUFJLEtBQUssTUFBekIsRUFBaUMsS0FBakMsRUFBc0M7QUFDbEMsd0NBQXNCLEdBQXRCLHVCQUEyQyxTQUEzQyxDQUFxRCx3QkFBc0IsR0FBdEIsYUFBckQsRUFEa0MsQ0FDdUQ7QUFDNUY7QUFDRDtBQUNBLGNBQUUscUJBQUYsRUFBeUIsSUFBekIsQ0FBOEIsU0FBUyxXQUFULEdBQXVCO0FBQ2pELG9CQUFNLE9BQU8sRUFBRSxJQUFGLEVBQVEsSUFBUixFQUFiO0FBQ0Esa0JBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxLQUFLLE9BQUwsQ0FBYSxPQUFiLEVBQXNCLE1BQXRCLENBQWI7QUFDSCxhQUhEO0FBSUEsY0FBRSxxQkFBRixFQUF5QixJQUF6QixDQUE4QixTQUFTLFdBQVQsR0FBdUI7QUFDakQsb0JBQU0sT0FBTyxFQUFFLElBQUYsRUFBUSxJQUFSLEVBQWI7QUFDQSxrQkFBRSxJQUFGLEVBQVEsSUFBUixDQUFhLEtBQUssT0FBTCxDQUFhLE1BQWIsRUFBcUIsS0FBckIsQ0FBYjtBQUNILGFBSEQ7QUFJQTtBQUNBLGNBQUUsc0RBQUYsRUFBMEQsSUFBMUQsQ0FBK0QsU0FBUyxXQUFULEdBQXVCO0FBQ2xGLG9CQUFNLE9BQU8sRUFBRSxJQUFGLEVBQVEsSUFBUixFQUFiO0FBQ0Esb0JBQUksS0FBSyxPQUFMLENBQWEsU0FBYixLQUEyQixDQUEvQixFQUFrQztBQUM5Qix3QkFBTSxRQUFRLEtBQUssS0FBTCxDQUFXLEdBQVgsQ0FBZDtBQUNBLHdCQUFNLFFBQVEsTUFBTSxDQUFOLEVBQVMsS0FBVCxDQUFlLEdBQWYsQ0FBZDtBQUNBLHNCQUFFLElBQUYsRUFBUSxRQUFSLENBQWlCLE1BQU0sQ0FBTixDQUFqQjtBQUNBLHNCQUFFLElBQUYsRUFBUSxJQUFSLENBQWMsTUFBTSxDQUFOLENBQUQsQ0FBVyxJQUFYLEVBQWI7QUFDSDtBQUNKLGFBUkQ7QUFTQTtBQUNBLGlCQUFLLElBQUksTUFBSSxDQUFiLEVBQWdCLE1BQUksU0FBUyxNQUE3QixFQUFxQyxFQUFFLEdBQXZDLEVBQTBDO0FBQ3RDLG9CQUFJLFNBQVMsTUFBVCxHQUFrQixDQUF0QixFQUF5QjtBQUNyQix3QkFBTSxXQUFXLFNBQVMsYUFBVCxDQUF1QixRQUF2QixDQUFqQjtBQUNBLDZCQUFTLFNBQVQsR0FBcUIsZUFBckI7QUFDQSw2QkFBUyxTQUFULEdBQXFCLFlBQXJCO0FBQ0EsNkJBQVMsZ0JBQVQsQ0FBMEIsT0FBMUIsRUFBbUMsU0FBbkM7QUFDQSxzQkFBRSxTQUFTLEdBQVQsQ0FBRixFQUFlLE1BQWYsQ0FBc0IsUUFBdEI7QUFDSDtBQUNKO0FBQ0Q7QUFDQSxvQkFBUSxTQUFSLENBQWtCLE1BQWxCLENBQXlCLE1BQXpCO0FBQ0E7QUFDQSxnQkFBTSxZQUFZLE9BQU8sU0FBekI7QUFDQSxnQkFBSSxDQUFFLFVBQVUsUUFBVixDQUFtQixNQUFuQixDQUFOLEVBQW1DLE9BQU8sU0FBUCxJQUFvQixNQUFwQjtBQUNuQyxnQkFBTSxlQUFlLFFBQVEsU0FBN0I7QUFDQSxnQkFBSSxhQUFhLFFBQWIsQ0FBc0IsTUFBdEIsQ0FBSixFQUFtQztBQUFFLHdCQUFRLFNBQVIsQ0FBa0IsTUFBbEIsQ0FBeUIsTUFBekI7QUFBbUM7QUFDM0U7QUFDSixLQTlERDtBQStEQSxXQUFPLFNBQVMsVUFBVCxDQUFvQixLQUFwQixFQUEyQjtBQUM5QixtQkFBVyxLQUFYO0FBQ0EsWUFBSSxTQUFTLEVBQWI7QUFDQSxpQkFBUyxPQUFPLEtBQVAsQ0FBVDtBQUNILEtBSkQ7QUFLSCxDQXJFa0IsRUFBbkI7O0FBdUVBLElBQU0sV0FBWSxZQUFNO0FBQ3BCLFdBQU8sU0FBUyxRQUFULENBQWtCLEtBQWxCLEVBQXlCO0FBQzVCLHFCQUFhLENBQWI7QUFDQSxZQUFJLE1BQU0sTUFBTixHQUFlLENBQW5CLEVBQXNCO0FBQ2xCLG9CQUFRLFNBQVIsR0FBb0IsRUFBcEI7QUFDQSx1QkFBVyxNQUFNLE1BQWpCO0FBQ0EsZ0JBQU0sWUFBWSxPQUFPLFNBQXpCO0FBQ0EsZ0JBQUksVUFBVSxRQUFWLENBQW1CLE1BQW5CLENBQUosRUFBZ0M7QUFDNUIsdUJBQU8sU0FBUCxDQUFpQixNQUFqQixDQUF3QixNQUF4QjtBQUNIO0FBQ0o7O0FBVDJCLG1DQVVuQixDQVZtQixFQVVaLENBVlk7QUFXeEIsZ0JBQU0sU0FBUyxJQUFJLFVBQUosRUFBZjtBQUNBLG1CQUFPLE1BQVAsR0FBZ0IsVUFBQyxDQUFELEVBQU87QUFDbkIsb0JBQUksT0FBTyxFQUFFLE1BQUYsQ0FBUyxNQUFwQjtBQUNBLHVCQUFPLElBQUksVUFBSixDQUFlLElBQWYsQ0FBUDtBQUNBLG9CQUFNLGNBQWMsTUFBTSxDQUFOLEVBQVMsSUFBVCxDQUFjLEtBQWQsQ0FBb0IsR0FBcEIsQ0FBcEI7QUFDQSw0QkFBWSxDQUFaLElBQWlCLFlBQVksQ0FBWixFQUFlLE9BQWYsQ0FBdUIsTUFBdkIsRUFBK0IsR0FBL0IsRUFBb0MsV0FBcEMsRUFBakI7QUFDQSx3QkFBUSxTQUFSLG9DQUFpRCxZQUFZLENBQVosQ0FBakQ7QUFDQSwyQkFBVyxTQUFTLElBQVQsQ0FBYyxJQUFkLEVBQW9CLEVBQUUsTUFBTSxPQUFSLEVBQXBCLENBQVg7QUFDQSxrQkFBRSxlQUFGLEVBQW1CLElBQW5CLENBQXdCLFNBQVMsVUFBVCxHQUFzQjtBQUMxQyx3QkFBTSxPQUFPLEVBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxlQUFiLENBQWI7QUFDQSx3QkFBSSxFQUFFLFFBQVEsU0FBUyxJQUFuQixDQUFKLEVBQThCO0FBQUUsMEJBQUUsSUFBRixFQUFRLElBQVIsQ0FBYSxlQUFiLEVBQThCLFlBQVksQ0FBWixDQUE5QjtBQUFnRDtBQUNuRixpQkFIRDtBQUlILGFBWEQ7QUFZQSxtQkFBTyxpQkFBUCxDQUF5QixDQUF6QjtBQXhCd0I7O0FBVTVCLGFBQUssSUFBSSxJQUFJLENBQVIsRUFBVyxDQUFoQixFQUFtQixJQUFJLE1BQU0sQ0FBTixDQUF2QixFQUFpQyxHQUFqQyxFQUFzQztBQUFBLGtCQUE3QixDQUE2QixFQUF0QixDQUFzQjtBQWVyQztBQUNKLEtBMUJEO0FBMkJILENBNUJnQixFQUFqQjs7QUE4QkE7QUFDQSxJQUFNLFlBQVksU0FBWixTQUFZLEdBQU07QUFDcEIsUUFBTSxNQUFNLFNBQVMsY0FBVCxDQUF3QixLQUF4QixDQUFaO0FBQ0EsUUFBSSxDQUFDLElBQUksZ0JBQVQsRUFBMkI7O0FBRTNCLGFBQVMsVUFBVCxDQUFvQixDQUFwQixFQUF1QjtBQUFFLGlCQUFTLEVBQUUsTUFBRixDQUFTLEtBQWxCO0FBQTJCO0FBQ3BELFFBQUksZ0JBQUosQ0FBcUIsUUFBckIsRUFBK0IsVUFBL0IsRUFBMkMsS0FBM0M7QUFDSCxDQU5EO0FBT0E7QUFDQTtBQUNBLFNBQVMsYUFBVCxDQUF1QixZQUF2QixFQUFxQyxnQkFBckMsQ0FBc0QsT0FBdEQsRUFBK0Q7QUFBQSxXQUFNLFNBQVMsY0FBVCxDQUF3QixLQUF4QixFQUErQixLQUEvQixFQUFOO0FBQUEsQ0FBL0Q7QUFDQTtBQUNBLFNBQVMsYUFBVCxDQUF1QixVQUF2QixFQUFtQyxnQkFBbkMsQ0FBb0QsT0FBcEQsRUFBNkQsWUFBTTtBQUMvRCxhQUFTLGFBQVQsQ0FBdUIsVUFBdkIsRUFBbUMsS0FBbkMsR0FBMkMsRUFBM0M7QUFDQSxRQUFJLFFBQVEsU0FBUixLQUFzQixFQUExQixFQUE4QjtBQUMxQixZQUFJLFlBQVksRUFBaEI7QUFDQSxZQUFNLGNBQWMsU0FBUyxnQkFBVCxDQUEwQixlQUExQixDQUFwQjtBQUNBLGFBQUssSUFBSSxJQUFJLENBQWIsRUFBZ0IsSUFBSSxZQUFZLE1BQWhDLEVBQXdDLEVBQUUsQ0FBMUMsRUFBNkM7QUFDekMscUNBQXVCLFlBQVksQ0FBWixFQUFlLFNBQXRDO0FBQ0g7QUFDRCxpQkFBUyxhQUFULENBQXVCLFVBQXZCLEVBQW1DLEtBQW5DLEdBQTJDLFNBQTNDO0FBQ0EsaUJBQVMsYUFBVCxDQUF1QixVQUF2QixFQUFtQyxNQUFuQztBQUNBLGlCQUFTLFdBQVQsQ0FBcUIsTUFBckI7QUFDSDtBQUNKLENBWkQ7QUFhQTtBQUNBLFNBQVMsU0FBVCxHQUFxQjtBQUNqQixhQUFTLGFBQVQsQ0FBdUIsVUFBdkIsRUFBbUMsS0FBbkMsR0FBMkMsRUFBM0M7QUFDQSxRQUFJLFFBQVEsU0FBUixLQUFzQixFQUExQixFQUE4QjtBQUMxQixZQUFJLFlBQVksRUFBaEI7QUFDQSxpQ0FBdUIsS0FBSyxXQUFMLENBQWlCLFNBQXhDO0FBQ0EsaUJBQVMsYUFBVCxDQUF1QixVQUF2QixFQUFtQyxLQUFuQyxHQUEyQyxTQUEzQztBQUNBLGlCQUFTLGFBQVQsQ0FBdUIsVUFBdkIsRUFBbUMsTUFBbkM7QUFDQSxpQkFBUyxXQUFULENBQXFCLE1BQXJCO0FBQ0g7QUFDSjtBQUNEO0FBQ0EsU0FBUyxhQUFULENBQXVCLGNBQXZCLEVBQXVDLGdCQUF2QyxDQUF3RCxPQUF4RCxFQUFpRSxZQUFNO0FBQ25FLFFBQUksUUFBUSxTQUFSLEtBQXNCLEVBQTFCLEVBQThCO0FBQzFCLFlBQUkseUJBQUo7QUFDQTtBQUNBLFlBQU0sY0FBYyxTQUFTLGdCQUFULENBQTBCLHNCQUExQixDQUFwQjtBQUNBLGFBQUssSUFBSSxJQUFJLENBQWIsRUFBZ0IsSUFBSSxZQUFZLE1BQWhDLEVBQXdDLEVBQUUsQ0FBMUMsRUFBNkM7QUFDekMsZ0JBQU0sY0FBYyxTQUFTLGdCQUFULGtDQUF5RCxZQUFZLENBQVosRUFBZSxTQUF4RSxPQUFwQjs7QUFEeUMseUNBR2hDLENBSGdDO0FBSXJDO0FBQ0Esb0JBQUksWUFBWSxNQUFaLEtBQXVCLENBQXZCLElBQTRCLFlBQVksTUFBWixLQUF1QixDQUF2RCxFQUEwRDtBQUN0RCx1Q0FBc0IsWUFBWSxDQUFaLEVBQWUsU0FBckMsU0FBa0QsWUFBWSxDQUFaLEVBQWUsZUFBZixDQUErQixTQUFqRjtBQUNILGlCQUZELE1BRU87QUFDSCx1Q0FBc0IsWUFBWSxDQUFaLEVBQWUsU0FBckMsU0FBa0QsWUFBWSxDQUFaLEVBQWUsZUFBZixDQUErQixlQUEvQixDQUErQyxTQUFqRztBQUNIO0FBQ0Qsb0JBQU0seUJBQXVCLFlBQVksQ0FBWixFQUFlLFNBQXRDLGFBQU47QUFDQTtBQUNBO0FBQ0Esb0JBQU0sbUJBQW1CLElBQUksSUFBSixDQUFTLENBQUMsVUFBRCxDQUFULEVBQXVCLEVBQUUsTUFBTSxZQUFSLEVBQXZCLENBQXpCO0FBQ0E7QUFDQTtBQUNBLG9CQUFNLGVBQWUsU0FBUyxhQUFULENBQXVCLEdBQXZCLENBQXJCO0FBQ0EsNkJBQWEsS0FBYixHQUFxQixlQUFyQjtBQUNBLHlCQUFTLElBQVQsQ0FBYyxXQUFkLENBQTBCLFlBQTFCO0FBQ0E7QUFDQTtBQUNBLG9CQUFNLGtCQUFrQixPQUFPLEdBQVAsQ0FBVyxlQUFYLENBQTJCLGdCQUEzQixDQUF4QjtBQUNBLDZCQUFhLElBQWIsR0FBb0IsZUFBcEI7QUFDQSw2QkFBYSxRQUFiLEdBQXdCLGdCQUF4QjtBQUNBLDZCQUFhLFNBQWIsR0FBeUIsZUFBekI7QUFDQTtBQUNBLDJCQUFXLFlBQU07QUFDYixpQ0FBYSxLQUFiO0FBQ0EsNEJBQVEsR0FBUixDQUFZLFVBQVo7QUFDQSw2QkFBUyxJQUFULENBQWMsV0FBZCxDQUEwQixZQUExQjtBQUNILGlCQUpELEVBSUcsSUFKSDtBQTFCcUM7O0FBR3pDLGlCQUFLLElBQUksSUFBSSxDQUFiLEVBQWdCLElBQUksWUFBWSxNQUFoQyxFQUF3QyxHQUF4QyxFQUE2QztBQUFBLHVCQUFwQyxDQUFvQztBQTRCNUM7QUFDSjtBQUNKLEtBckNELE1BcUNPO0FBQ0gsY0FBTSx3REFBTjtBQUNIO0FBQ0osQ0F6Q0QiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpe2Z1bmN0aW9uIHIoZSxuLHQpe2Z1bmN0aW9uIG8oaSxmKXtpZighbltpXSl7aWYoIWVbaV0pe3ZhciBjPVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmU7aWYoIWYmJmMpcmV0dXJuIGMoaSwhMCk7aWYodSlyZXR1cm4gdShpLCEwKTt2YXIgYT1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK2krXCInXCIpO3Rocm93IGEuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixhfXZhciBwPW5baV09e2V4cG9ydHM6e319O2VbaV1bMF0uY2FsbChwLmV4cG9ydHMsZnVuY3Rpb24ocil7dmFyIG49ZVtpXVsxXVtyXTtyZXR1cm4gbyhufHxyKX0scCxwLmV4cG9ydHMscixlLG4sdCl9cmV0dXJuIG5baV0uZXhwb3J0c31mb3IodmFyIHU9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZSxpPTA7aTx0Lmxlbmd0aDtpKyspbyh0W2ldKTtyZXR1cm4gb31yZXR1cm4gcn0pKCkiLCIvLyBWYXJpYWJsZXNcclxuY29uc3QgZXhjZWxEb2MgPSBYTFNYO1xyXG5jb25zdCBIVE1MT1VUID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ291dHB1dCcpO1xyXG5jb25zdCBidG5Db3B5ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2J0bkNvcHknKTtcclxuY29uc3QgbG9hZGVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2xvYWRlcicpO1xyXG5cclxubGV0IGdsb2JhbFdiO1xyXG5sZXQgaHRtbHN0cjtcclxubGV0IGZpbGVzTGVuO1xyXG5sZXQgdG90YWxDb3VudDtcclxuXHJcbi8vIERlZmluZSBuZXcgdmFsdWVzXHJcbnRvdGFsQ291bnQgPSAwO1xyXG5IVE1MT1VULmlubmVySFRNTCA9ICcnO1xyXG4vLyBEb2N1bWVudCBQcm9jZXNzaW5nLCBodG1sIHRhYmxlIGNvZGUgc3RvcmVkIGluIHRoZSB2YXJpYWJsZSBhbmQgZGlzcGxheWVkIG9uIHRoZSBzY3JlZW5cclxuY29uc3QgcHJvY2Vzc0RvYyA9ICgoKSA9PiB7XHJcbiAgICBjb25zdCB0b0h0bWwgPSBmdW5jdGlvbiB0b0h0bWwod29ya2Jvb2spIHtcclxuICAgICAgICB3b3JrYm9vay5TaGVldE5hbWVzLmZvckVhY2goKHNoZWV0TmFtZSkgPT4ge1xyXG4gICAgICAgICAgICBodG1sc3RyID0gYDxkaXYgY2xhc3M9J3NoZWV0bmFtZSc+JHtzaGVldE5hbWV9PC9kaXY+YDtcclxuICAgICAgICAgICAgaHRtbHN0ciArPSBleGNlbERvYy53cml0ZSh3b3JrYm9vaywgeyBzaGVldDogc2hlZXROYW1lLCB0eXBlOiAnYmluYXJ5JywgYm9va1R5cGU6ICdodG1sJyB9KTtcclxuICAgICAgICAgICAgSFRNTE9VVC5pbm5lckhUTUwgKz0gaHRtbHN0cjtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0b3RhbENvdW50Kys7XHJcbiAgICAgICAgaWYgKHRvdGFsQ291bnQgPT09IGZpbGVzTGVuKSB7XHJcbiAgICAgICAgICAgIC8vIHJlbW92aW5nIHRoZSBwcmUtZ2VuZXJhdGVkICdpZCcgYW5kICd0JyBhdHRyaWJ1dGVzIGluc2lkZSB0aGUgdGFibGUgY2VsbHNcclxuICAgICAgICAgICAgY29uc3QgZWxlbXMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcjb3V0cHV0IHRhYmxlIHRyIHRkJyk7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZWxlbXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGVsZW1zW2ldLnJlbW92ZUF0dHJpYnV0ZSgnaWQnKTtcclxuICAgICAgICAgICAgICAgIGVsZW1zW2ldLnJlbW92ZUF0dHJpYnV0ZSgndCcpO1xyXG4gICAgICAgICAgICAgICAgZWxlbXNbaV0ucmVtb3ZlQXR0cmlidXRlKCd4bWw6c3BhY2UnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyBjcmVhdGluZyBUSCBlbGVtZW50IGluIGVhY2ggYW5kIGV2ZXJ5IHRhYmxlc1xyXG4gICAgICAgICAgICBjb25zdCB0YWJsZUFsbCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdvdXRwdXQnKS5xdWVyeVNlbGVjdG9yQWxsKCd0YWJsZScpO1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRhYmxlQWxsLmxlbmd0aDsgKytpKSB7XHJcbiAgICAgICAgICAgICAgICB0YWJsZUFsbFtpXS5jcmVhdGVUSGVhZCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIE1vdmluZyBmaXJzdCByb3cgZnJvbSBUYm9keSBzZWN0aW9uIHRvIFRIZWFkIHNlY3Rpb24gaW4gdGhlIHRhYmxlXHJcbiAgICAgICAgICAgIGNvbnN0IGRpdnMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcjb3V0cHV0IHRhYmxlJyk7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZGl2cy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgJChgI291dHB1dCB0YWJsZTplcSgke2l9KSB0Ym9keSB0cjplcSgwKWApLnByZXBlbmRUbygkKGAjb3V0cHV0IHRhYmxlOmVxKCR7aX0pIHRoZWFkYCkpOyAvLyBtb3ZlIGZpcnN0IHRyIHRvIHRoZWFkXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gUmVwbGFjaW5nIFREIHdpdGggVEggaW4gZXZlcnkgcm93IGluc2lkZSBUaGVhZCBzZWN0aW9uXHJcbiAgICAgICAgICAgICQoJyNvdXRwdXQgdGFibGUgdGhlYWQnKS5lYWNoKGZ1bmN0aW9uIHJlcGxhY2VUZXh0KCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdGV4dCA9ICQodGhpcykuaHRtbCgpO1xyXG4gICAgICAgICAgICAgICAgJCh0aGlzKS5odG1sKHRleHQucmVwbGFjZSgvPHRkIC9nLCAnPHRoICcpKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICQoJyNvdXRwdXQgdGFibGUgdGhlYWQnKS5lYWNoKGZ1bmN0aW9uIHJlcGxhY2VUZXh0KCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdGV4dCA9ICQodGhpcykuaHRtbCgpO1xyXG4gICAgICAgICAgICAgICAgJCh0aGlzKS5odG1sKHRleHQucmVwbGFjZSgvdGQ+L2csICd0aD4nKSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAvLyBBZGRpbmcgY2xhc3MgbmFtZXMgZHluYW1pY2FsbHkgaWYgdGhlIGNlbGwgdmFsdWVzIGNvbnRhaW4gJ3tjbGFzczonIGluIHRoZSB0YWJsZVxyXG4gICAgICAgICAgICAkKCcjb3V0cHV0IHRhYmxlIHRoZWFkIHRyIHRoLCAjb3V0cHV0IHRhYmxlIHRib2R5IHRyIHRkJykuZWFjaChmdW5jdGlvbiByZXBsYWNlVGV4dCgpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRleHQgPSAkKHRoaXMpLmh0bWwoKTtcclxuICAgICAgICAgICAgICAgIGlmICh0ZXh0LmluZGV4T2YoJ3tjbGFzczonKSA+PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdGRWYWwgPSB0ZXh0LnNwbGl0KCc6Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZmluYWwgPSB0ZFZhbFsxXS5zcGxpdCgnfScpO1xyXG4gICAgICAgICAgICAgICAgICAgICQodGhpcykuYWRkQ2xhc3MoZmluYWxbMF0pO1xyXG4gICAgICAgICAgICAgICAgICAgICQodGhpcykuaHRtbCgoZmluYWxbMV0pLnRyaW0oKSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAvLyBDcmVhdGluZyBjb3B5IGJ1dHRvbnMgZm9yIGVhY2ggdGFibGUgb24gdGhlIHNjcmVlblxyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRhYmxlQWxsLmxlbmd0aDsgKytpKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGFibGVBbGwubGVuZ3RoID4gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGJ0blRhYmxlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYnV0dG9uJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnRuVGFibGUuY2xhc3NOYW1lID0gJ2NvcHlUYWJsZSBidG4nO1xyXG4gICAgICAgICAgICAgICAgICAgIGJ0blRhYmxlLmlubmVySFRNTCA9ICdDb3B5IFRhYmxlJztcclxuICAgICAgICAgICAgICAgICAgICBidG5UYWJsZS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGNvcHlUYWJsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgJCh0YWJsZUFsbFtpXSkuYmVmb3JlKGJ0blRhYmxlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyBEaXNwbGF5aW5nIHRoZSBmaW5hbCBvdXRwdXQgb24gdGhlIHNjcmVlblxyXG4gICAgICAgICAgICBIVE1MT1VULmNsYXNzTGlzdC5yZW1vdmUoJ2hpZGUnKTtcclxuICAgICAgICAgICAgLy8gSGlkaW5nIGxvYWRlciBpY29uIG9uY2UgdGhlIGZpbmFsIG91dHB1dCBpcyBzaG93biBvbiB0aGUgc2NyZWVuXHJcbiAgICAgICAgICAgIGNvbnN0IGxvYWRlckNscyA9IGxvYWRlci5jbGFzc0xpc3Q7XHJcbiAgICAgICAgICAgIGlmICghKGxvYWRlckNscy5jb250YWlucygnaGlkZScpKSkgbG9hZGVyLmNsYXNzTmFtZSArPSAnaGlkZSc7XHJcbiAgICAgICAgICAgIGNvbnN0IGJ0bkNvcHljbGFzcyA9IGJ0bkNvcHkuY2xhc3NMaXN0O1xyXG4gICAgICAgICAgICBpZiAoYnRuQ29weWNsYXNzLmNvbnRhaW5zKCdoaWRlJykpIHsgYnRuQ29weS5jbGFzc0xpc3QucmVtb3ZlKCdoaWRlJyk7IH1cclxuICAgICAgICB9XHJcbiAgICB9O1xyXG4gICAgcmV0dXJuIGZ1bmN0aW9uIHByb2Nlc3NEb2Mod0Jvb2spIHtcclxuICAgICAgICBnbG9iYWxXYiA9IHdCb29rO1xyXG4gICAgICAgIGxldCBvdXRwdXQgPSAnJztcclxuICAgICAgICBvdXRwdXQgPSB0b0h0bWwod0Jvb2spO1xyXG4gICAgfTtcclxufSkoKTtcclxuXHJcbmNvbnN0IHJlYWRGaWxlID0gKCgpID0+IHtcclxuICAgIHJldHVybiBmdW5jdGlvbiByZWFkRmlsZShmaWxlcykge1xyXG4gICAgICAgIHRvdGFsQ291bnQgPSAwO1xyXG4gICAgICAgIGlmIChmaWxlcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIEhUTUxPVVQuaW5uZXJIVE1MID0gJyc7XHJcbiAgICAgICAgICAgIGZpbGVzTGVuID0gZmlsZXMubGVuZ3RoO1xyXG4gICAgICAgICAgICBjb25zdCBsb2FkZXJDbHMgPSBsb2FkZXIuY2xhc3NMaXN0O1xyXG4gICAgICAgICAgICBpZiAobG9hZGVyQ2xzLmNvbnRhaW5zKCdoaWRlJykpIHtcclxuICAgICAgICAgICAgICAgIGxvYWRlci5jbGFzc0xpc3QucmVtb3ZlKCdoaWRlJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDAsIGY7IGYgPSBmaWxlc1tpXTsgaSsrKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XHJcbiAgICAgICAgICAgIHJlYWRlci5vbmxvYWQgPSAoZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgbGV0IGRhdGEgPSBlLnRhcmdldC5yZXN1bHQ7XHJcbiAgICAgICAgICAgICAgICBkYXRhID0gbmV3IFVpbnQ4QXJyYXkoZGF0YSk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBmaWxlQ2FwdGlvbiA9IGZpbGVzW2ldLm5hbWUuc3BsaXQoJy4nKTtcclxuICAgICAgICAgICAgICAgIGZpbGVDYXB0aW9uWzBdID0gZmlsZUNhcHRpb25bMF0ucmVwbGFjZSgvXFxzKy9nLCAnLScpLnRvTG93ZXJDYXNlKCk7XHJcbiAgICAgICAgICAgICAgICBIVE1MT1VULmlubmVySFRNTCArPSBgPGRpdiBjbGFzcz0nZmlsZUNhcHRpb24nPiR7ZmlsZUNhcHRpb25bMF19PC9kaXY+YDtcclxuICAgICAgICAgICAgICAgIHByb2Nlc3NEb2MoZXhjZWxEb2MucmVhZChkYXRhLCB7IHR5cGU6ICdhcnJheScgfSkpO1xyXG4gICAgICAgICAgICAgICAgJCgnI291dHB1dCB0YWJsZScpLmVhY2goZnVuY3Rpb24gYWRkRWxlbWVudCgpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBhdHRyID0gJCh0aGlzKS5hdHRyKCdkYXRhLWZpbGVuYW1lJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEoYXR0ciAmJiBhdHRyICE9PSB0cnVlKSkgeyAkKHRoaXMpLmF0dHIoJ2RhdGEtZmlsZW5hbWUnLCBmaWxlQ2FwdGlvblswXSk7IH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICByZWFkZXIucmVhZEFzQXJyYXlCdWZmZXIoZik7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxufSkoKTtcclxuXHJcbi8vIFJlbW92ZSBmaWxlIGV4dGVuc2lvbiBmcm9tIHRoZSBmaWxlIG5hbWUgJiBzdG9yZSBpbiB0aGUgdGV4dGJveFxyXG5jb25zdCBleGNlbEZpbGUgPSAoKSA9PiB7XHJcbiAgICBjb25zdCB4bGYgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgneGxmJyk7XHJcbiAgICBpZiAoIXhsZi5hZGRFdmVudExpc3RlbmVyKSByZXR1cm47XHJcblxyXG4gICAgZnVuY3Rpb24gaGFuZGxlRmlsZShlKSB7IHJlYWRGaWxlKGUudGFyZ2V0LmZpbGVzKTsgfVxyXG4gICAgeGxmLmFkZEV2ZW50TGlzdGVuZXIoJ2NoYW5nZScsIGhhbmRsZUZpbGUsIGZhbHNlKTtcclxufTtcclxuZXhjZWxGaWxlKCk7XHJcbi8vIEFzc2lnbiBldmVuIGxpc3RlbmVyIHRvIHRoZSBicm93c2UgYnV0dG9uIHRvIGVuYWJsZSBpdCB0byBjbGljayB0aGUgZmlsZSBpbnB1dCBib3guIEZpbGUgaW5wdXQgYm94IGlzIGtlcHQgaGlkZGVuIGZvciBVSSBkZWNvcmF0aW9uIHB1cnBvc2UuXHJcbmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNicm93c2VCdG4nKS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCd4bGYnKS5jbGljaygpKTtcclxuLy8gQ29weSBidXR0b24gZnVuY3Rpb24gdG8gY29weSB0aGUgY29kZSBmcm9tIHRoZSBvdXRwdXQgZWxlbWVudCBESVZcclxuZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI2J0bkNvcHknKS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IHtcclxuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNjb3B5VHh0JykudmFsdWUgPSAnJztcclxuICAgIGlmIChIVE1MT1VULmlubmVySFRNTCAhPT0gJycpIHtcclxuICAgICAgICBsZXQgZmluYWxDb3B5ID0gJyc7XHJcbiAgICAgICAgY29uc3Qgb3V0cHV0VGFibGUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcjb3V0cHV0IHRhYmxlJyk7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBvdXRwdXRUYWJsZS5sZW5ndGg7ICsraSkge1xyXG4gICAgICAgICAgICBmaW5hbENvcHkgKz0gYDx0YWJsZT4ke291dHB1dFRhYmxlW2ldLmlubmVySFRNTH08L3RhYmxlPmA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNjb3B5VHh0JykudmFsdWUgPSBmaW5hbENvcHk7XHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI2NvcHlUeHQnKS5zZWxlY3QoKTtcclxuICAgICAgICBkb2N1bWVudC5leGVjQ29tbWFuZCgnQ29weScpO1xyXG4gICAgfVxyXG59KTtcclxuLy8gQ29weVRhYmxlIGZ1bmN0aW9uIHRvIGNvcHkgdGhlIGluZGl2aWR1YWwgdGFibGUgY29kZVxyXG5mdW5jdGlvbiBjb3B5VGFibGUoKSB7XHJcbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjY29weVR4dCcpLnZhbHVlID0gJyc7XHJcbiAgICBpZiAoSFRNTE9VVC5pbm5lckhUTUwgIT09ICcnKSB7XHJcbiAgICAgICAgbGV0IGZpbmFsQ29weSA9ICcnO1xyXG4gICAgICAgIGZpbmFsQ29weSArPSBgPHRhYmxlPiR7dGhpcy5uZXh0U2libGluZy5pbm5lckhUTUx9PC90YWJsZT5gO1xyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNjb3B5VHh0JykudmFsdWUgPSBmaW5hbENvcHk7XHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI2NvcHlUeHQnKS5zZWxlY3QoKTtcclxuICAgICAgICBkb2N1bWVudC5leGVjQ29tbWFuZCgnQ29weScpO1xyXG4gICAgfVxyXG59XHJcbi8vIERvd25sb2FkIGNvZGUgaW4gdGV4dCBmaWxlIGZvcm1hdCB3aGVuIGNsaWNraW5nIGRvd25sb2FkIGJ1dHRvblxyXG5kb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjYnRuRG93bmxvYWQnKS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICgpID0+IHtcclxuICAgIGlmIChIVE1MT1VULmlubmVySFRNTCAhPT0gJycpIHtcclxuICAgICAgICBsZXQgZmlsZU5hbWVUb1NhdmVBcztcclxuICAgICAgICAvLyBHZXQgdGhlIGVsZW1lbnQgd2l0aCBpZD1cIm15RElWXCIgKGEgZGl2KSwgdGhlbiBnZXQgYWxsIHAgZWxlbWVudHMgd2l0aCBjbGFzcz1cImV4YW1wbGVcIiBpbnNpZGUgZGl2XHJcbiAgICAgICAgY29uc3QgZmlsZUNhcHRpb24gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcjb3V0cHV0IC5maWxlQ2FwdGlvbicpO1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZmlsZUNhcHRpb24ubGVuZ3RoOyArK2kpIHtcclxuICAgICAgICAgICAgY29uc3Qgb3V0cHV0VGFibGUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKGAjb3V0cHV0IHRhYmxlW2RhdGEtZmlsZW5hbWU9JHtmaWxlQ2FwdGlvbltpXS5pbm5lckhUTUx9XWApO1xyXG5cclxuICAgICAgICAgICAgZm9yIChsZXQgaiA9IDA7IGogPCBvdXRwdXRUYWJsZS5sZW5ndGg7IGorKykge1xyXG4gICAgICAgICAgICAgICAgLy8gRmlsZSBuYW1lcyB3aWxsIGJlIGtlcHQgYXMgIGZpbGUgYW5kIHNoZWV0IG5hbWUgZm9yIGJldHRlciB1bmRlcnN0YW5kaW5nXHJcbiAgICAgICAgICAgICAgICBpZiAoZmlsZUNhcHRpb24ubGVuZ3RoID09PSAxICYmIG91dHB1dFRhYmxlLmxlbmd0aCA9PT0gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGZpbGVOYW1lVG9TYXZlQXMgPSBgJHtmaWxlQ2FwdGlvbltpXS5pbm5lckhUTUx9LSR7b3V0cHV0VGFibGVbal0ucHJldmlvdXNTaWJsaW5nLmlubmVySFRNTH1gO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBmaWxlTmFtZVRvU2F2ZUFzID0gYCR7ZmlsZUNhcHRpb25baV0uaW5uZXJIVE1MfS0ke291dHB1dFRhYmxlW2pdLnByZXZpb3VzU2libGluZy5wcmV2aW91c1NpYmxpbmcuaW5uZXJIVE1MfWA7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBjb25zdCB0ZXh0VG9TYXZlID0gYDx0YWJsZT4ke291dHB1dFRhYmxlW2pdLmlubmVySFRNTH08L3RhYmxlPmA7XHJcbiAgICAgICAgICAgICAgICAvLyBDcmVhdGUgYSBuZXcgQmxvYiBvYmplY3QgdXNpbmcgdGhlXHJcbiAgICAgICAgICAgICAgICAvL3Jlc3BvbnNlIGRhdGEgb2YgdGhlIG9ubG9hZCBvYmplY3RcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRleHRUb1NhdmVBc0Jsb2IgPSBuZXcgQmxvYihbdGV4dFRvU2F2ZV0sIHsgdHlwZTogJ3RleHQvcGxhaW4nIH0pO1xyXG4gICAgICAgICAgICAgICAgLy9DcmVhdGUgYSBsaW5rIGVsZW1lbnQsIGhpZGUgaXQsIGRpcmVjdFxyXG4gICAgICAgICAgICAgICAgLy9pdCB0b3dhcmRzIHRoZSBibG9iLCBhbmQgdGhlbiAnY2xpY2snIGl0IHByb2dyYW1hdGljYWxseVxyXG4gICAgICAgICAgICAgICAgY29uc3QgZG93bmxvYWRMaW5rID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpO1xyXG4gICAgICAgICAgICAgICAgZG93bmxvYWRMaW5rLnN0eWxlID0gXCJkaXNwbGF5OiBub25lXCI7XHJcbiAgICAgICAgICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKGRvd25sb2FkTGluayk7XHJcbiAgICAgICAgICAgICAgICAvL0NyZWF0ZSBhIERPTVN0cmluZyByZXByZXNlbnRpbmcgdGhlIGJsb2JcclxuICAgICAgICAgICAgICAgIC8vYW5kIHBvaW50IHRoZSBsaW5rIGVsZW1lbnQgdG93YXJkcyBpdFxyXG4gICAgICAgICAgICAgICAgY29uc3QgdGV4dFRvU2F2ZUFzVVJMID0gd2luZG93LlVSTC5jcmVhdGVPYmplY3RVUkwodGV4dFRvU2F2ZUFzQmxvYik7XHJcbiAgICAgICAgICAgICAgICBkb3dubG9hZExpbmsuaHJlZiA9IHRleHRUb1NhdmVBc1VSTDtcclxuICAgICAgICAgICAgICAgIGRvd25sb2FkTGluay5kb3dubG9hZCA9IGZpbGVOYW1lVG9TYXZlQXM7XHJcbiAgICAgICAgICAgICAgICBkb3dubG9hZExpbmsuaW5uZXJIVE1MID0gJ0Rvd25sb2FkIEZpbGUnO1xyXG4gICAgICAgICAgICAgICAgLy9wcm9ncmFtYXRpY2FsbHkgY2xpY2sgdGhlIGxpbmsgdG8gdHJpZ2dlciB0aGVcclxuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGRvd25sb2FkTGluay5jbGljaygpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRG93bmxvYWRcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQuYm9keS5yZW1vdmVDaGlsZChkb3dubG9hZExpbmspO1xyXG4gICAgICAgICAgICAgICAgfSwgMTAwMCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIGFsZXJ0KCdQbGVhc2Ugc2VsZWN0IHRoZSBmaWxlIHlvdSB3YW50IHRvIGNvbnZlcnQgdG8gYSB0YWJsZS4nKTtcclxuICAgIH1cclxufSk7Il19
