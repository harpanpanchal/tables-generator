Table Generator: 

This Application is useful to convert Excel documents into Table format (HTML) and can be downloaded for further use.

Simply upload Excel document and either copy table by clicking Copy button or download button to download HTML table in the text format.

This application uses some third party libraries and custom Javascript.
