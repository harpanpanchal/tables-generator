// Variables
const excelDoc = XLSX;
const HTMLOUT = document.getElementById('output');
const btnCopy = document.getElementById('btnCopy');
const loader = document.getElementById('loader');

let globalWb;
let htmlstr;
let filesLen;
let totalCount;

// Define new values
totalCount = 0;
HTMLOUT.innerHTML = '';
// Document Processing, html table code stored in the variable and displayed on the screen
const processDoc = (() => {
    const toHtml = function toHtml(workbook) {
        workbook.SheetNames.forEach((sheetName) => {
            htmlstr = `<div class='sheetname'>${sheetName}</div>`;
            htmlstr += excelDoc.write(workbook, { sheet: sheetName, type: 'binary', bookType: 'html' });
            HTMLOUT.innerHTML += htmlstr;
        });
        totalCount++;
        if (totalCount === filesLen) {
            // removing the pre-generated 'id' and 't' attributes inside the table cells
            const elems = document.querySelectorAll('#output table tr td');
            for (let i = 0; i < elems.length; i++) {
                elems[i].removeAttribute('id');
                elems[i].removeAttribute('t');
                elems[i].removeAttribute('xml:space');
            }
            // creating TH element in each and every tables
            const tableAll = document.getElementById('output').querySelectorAll('table');
            for (let i = 0; i < tableAll.length; ++i) {
                tableAll[i].createTHead();
            }
            // Moving first row from Tbody section to THead section in the table
            const divs = document.querySelectorAll('#output table');
            for (let i = 0; i < divs.length; i++) {
                $(`#output table:eq(${i}) tbody tr:eq(0)`).prependTo($(`#output table:eq(${i}) thead`)); // move first tr to thead
            }
            // Replacing TD with TH in every row inside Thead section
            $('#output table thead').each(function replaceText() {
                const text = $(this).html();
                $(this).html(text.replace(/<td /g, '<th '));
            });
            $('#output table thead').each(function replaceText() {
                const text = $(this).html();
                $(this).html(text.replace(/td>/g, 'th>'));
            });
            // Adding class names dynamically if the cell values contain '{class:' in the table
            $('#output table thead tr th, #output table tbody tr td').each(function replaceText() {
                const text = $(this).html();
                if (text.indexOf('{class:') >= 0) {
                    const tdVal = text.split(':');
                    const final = tdVal[1].split('}');
                    $(this).addClass(final[0]);
                    $(this).html((final[1]).trim());
                }
            });
            // Creating copy buttons for each table on the screen
            for (let i = 0; i < tableAll.length; ++i) {
                if (tableAll.length > 1) {
                    const btnTable = document.createElement('button');
                    btnTable.className = 'copyTable btn';
                    btnTable.innerHTML = 'Copy Table';
                    btnTable.addEventListener('click', copyTable);
                    $(tableAll[i]).before(btnTable);
                }
            }
            // Displaying the final output on the screen
            HTMLOUT.classList.remove('hide');
            // Hiding loader icon once the final output is shown on the screen
            const loaderCls = loader.classList;
            if (!(loaderCls.contains('hide'))) loader.className += 'hide';
            const btnCopyclass = btnCopy.classList;
            if (btnCopyclass.contains('hide')) { btnCopy.classList.remove('hide'); }
        }
    };
    return function processDoc(wBook) {
        globalWb = wBook;
        let output = '';
        output = toHtml(wBook);
    };
})();

const readFile = (() => {
    return function readFile(files) {
        totalCount = 0;
        if (files.length > 0) {
            HTMLOUT.innerHTML = '';
            filesLen = files.length;
            const loaderCls = loader.classList;
            if (loaderCls.contains('hide')) {
                loader.classList.remove('hide');
            }
        }
        for (let i = 0, f; f = files[i]; i++) {
            const reader = new FileReader();
            reader.onload = (e) => {
                let data = e.target.result;
                data = new Uint8Array(data);
                const fileCaption = files[i].name.split('.');
                fileCaption[0] = fileCaption[0].replace(/\s+/g, '-').toLowerCase();
                HTMLOUT.innerHTML += `<div class='fileCaption'>${fileCaption[0]}</div>`;
                processDoc(excelDoc.read(data, { type: 'array' }));
                $('#output table').each(function addElement() {
                    const attr = $(this).attr('data-filename');
                    if (!(attr && attr !== true)) { $(this).attr('data-filename', fileCaption[0]); }
                });
            };
            reader.readAsArrayBuffer(f);
        }
    };
})();

// Remove file extension from the file name & store in the textbox
const excelFile = () => {
    const xlf = document.getElementById('xlf');
    if (!xlf.addEventListener) return;

    function handleFile(e) { readFile(e.target.files); }
    xlf.addEventListener('change', handleFile, false);
};
excelFile();
// Assign even listener to the browse button to enable it to click the file input box. File input box is kept hidden for UI decoration purpose.
document.querySelector('#browseBtn').addEventListener('click', () => document.getElementById('xlf').click());
// Copy button function to copy the code from the output element DIV
document.querySelector('#btnCopy').addEventListener('click', () => {
    document.querySelector('#copyTxt').value = '';
    if (HTMLOUT.innerHTML !== '') {
        let finalCopy = '';
        const outputTable = document.querySelectorAll('#output table');
        for (let i = 0; i < outputTable.length; ++i) {
            finalCopy += `<table>${outputTable[i].innerHTML}</table>`;
        }
        document.querySelector('#copyTxt').value = finalCopy;
        document.querySelector('#copyTxt').select();
        document.execCommand('Copy');
    }
});
// CopyTable function to copy the individual table code
function copyTable() {
    document.querySelector('#copyTxt').value = '';
    if (HTMLOUT.innerHTML !== '') {
        let finalCopy = '';
        finalCopy += `<table>${this.nextSibling.innerHTML}</table>`;
        document.querySelector('#copyTxt').value = finalCopy;
        document.querySelector('#copyTxt').select();
        document.execCommand('Copy');
    }
}
// Download code in text file format when clicking download button
document.querySelector('#btnDownload').addEventListener('click', () => {
    if (HTMLOUT.innerHTML !== '') {
        let fileNameToSaveAs;
        // Get the element with id="myDIV" (a div), then get all p elements with class="example" inside div
        const fileCaption = document.querySelectorAll('#output .fileCaption');
        for (let i = 0; i < fileCaption.length; ++i) {
            const outputTable = document.querySelectorAll(`#output table[data-filename=${fileCaption[i].innerHTML}]`);

            for (let j = 0; j < outputTable.length; j++) {
                // File names will be kept as  file and sheet name for better understanding
                if (fileCaption.length === 1 && outputTable.length === 1) {
                    fileNameToSaveAs = `${fileCaption[i].innerHTML}-${outputTable[j].previousSibling.innerHTML}`;
                } else {
                    fileNameToSaveAs = `${fileCaption[i].innerHTML}-${outputTable[j].previousSibling.previousSibling.innerHTML}`;
                }
                const textToSave = `<table>${outputTable[j].innerHTML}</table>`;
                // Create a new Blob object using the
                //response data of the onload object
                const textToSaveAsBlob = new Blob([textToSave], { type: 'text/plain' });
                //Create a link element, hide it, direct
                //it towards the blob, and then 'click' it programatically
                const downloadLink = document.createElement('a');
                downloadLink.style = "display: none";
                document.body.appendChild(downloadLink);
                //Create a DOMString representing the blob
                //and point the link element towards it
                const textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);
                downloadLink.href = textToSaveAsURL;
                downloadLink.download = fileNameToSaveAs;
                downloadLink.innerHTML = 'Download File';
                //programatically click the link to trigger the
                setTimeout(() => {
                    downloadLink.click();
                    console.log("Download");
                    document.body.removeChild(downloadLink);
                }, 1000);
            }
        }
    } else {
        alert('Please select the file you want to convert to a table.');
    }
});