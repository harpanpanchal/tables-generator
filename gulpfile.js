var gulp = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
   uglify = require('gulp-uglify'),
    pump = require('pump'),
    browserify = require('browserify'),
    babelify = require('babelify'),
    source = require('vinyl-source-stream'),
    sassSources = ['styles/*.scss'],
    htmlSources = ['**/*.html'],
    es6Js = 'scripts/source/main.js',
    bs = require('browser-sync').create();

/*gulp.task('copy', function() {
    gulp.src('scripts/lib/*.js')
        .pipe(gulp.dest('assets/js'));
});
*/

gulp.task('browser-sync', ['sass'], function() {
    bs.init({
        server: {
            baseDir: "./"
        }
    });
});


gulp.task('compress', function() {
    pump([
        gulp.src('scripts/lib/*.js'),
        uglify(),
        gulp.dest('assets/js')
    ]);
});


gulp.task('build', function() {
    return browserify({
            entries: es6Js,
            extensions: ['.js'],
            debug: true
        })
        .transform("babelify", { presets: ["es2015"] })
        .bundle()
        .pipe(source('main.js'))
        .pipe(gulp.dest('assets/js'))
        .pipe(bs.reload({ stream: true }));
});
gulp.task('sass', function() {
    gulp.src(sassSources)
        .pipe(sass({ style: 'expanded' }))
        .on('error', gutil.log)
        .pipe(gulp.dest('assets/css/'))
        .pipe(bs.reload({ stream: true }));
});
/*gulp.task('watch', function() {
    gulp.watch(sassSources, ['sass']);
    gulp.watch(htmlSources, ['html']);
    gulp.watch(es6Js, ['build']);
});
*/
gulp.task('watch', ['browser-sync'], function() {
    gulp.watch(sassSources, ['sass']);
    gulp.watch(es6Js, ['build']);
    gulp.watch("*.html").on('change', bs.reload);
});


/*

gulp.task('html', function() {
    gulp.src(htmlSources)
        .pipe(connect.reload());
});


gulp.task('connect', function() {
    connect.server({
        root: '.',
        livereload: true,
        port: 8080
    });
});
*/

gulp.task('default', ['compress', 'build', 'sass', 'watch', 'browser-sync']);
